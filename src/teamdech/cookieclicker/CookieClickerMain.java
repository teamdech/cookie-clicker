/**************************************************************************************************
 * cookieclicker Copyright (C) 2014 Team DE-CH: domi1819 / Joel Messerli                          *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package teamdech.cookieclicker;

import ch.jmnetwork.core.logging.Logger;
import ch.jmnetwork.core.logging.LogLevel;
import ch.jmnetwork.core.tools.network.FileDownloaderThread;
import ch.jmnetwork.core.tools.network.NetworkUtils;
import teamdech.cookieclicker.achievement.AchievementManager;
import teamdech.cookieclicker.achievement.CheckAchievementsThread;
import teamdech.cookieclicker.helper.Helper;
import teamdech.cookieclicker.threading.CheckButtonsThread;
import teamdech.cookieclicker.threading.SaveThread;
import teamdech.cookieclicker.ui.CCUserInterface;
import teamdech.cookieclicker.util.SaveLoadHandler;

import java.io.File;

import static teamdech.cookieclicker.ui.CCUserInterface.LabelType.*;

public class CookieClickerMain {

    public static CookieClickerMain instance;

    public CookieManager cookieManager = new CookieManager();
    public CCUserInterface theInterface;
    public AchievementManager achievementManager = new AchievementManager();
    public SaveLoadHandler slHandler = new SaveLoadHandler(cookieManager);
    public Logger logger = new Logger(new File("./cookieclicker.log"), "Cookie Clicker");
    public CheckButtonsThread checkButtonsThread;

    public int tickCounter = 0;
    public long lastHandmadeCookies = 0, thisHandmadeCookies = 0, handmadePerSec = 0;


    public static void main(String[] args) {
        getResources();

        instance = new CookieClickerMain();
        instance.checkButtonsThread = new CheckButtonsThread();

        instance.startGame();
    }

    private void startGame() {
        //DEBUG TODO set to LogLevel.ERROR for release
        logger.setDisplayedLogLevel(LogLevel.ALL);

        theInterface = new CCUserInterface(cookieManager, slHandler);
        slHandler.loadFromDisk();

        getResources();

        long last = 0, current;

        while (true) {

            current = System.nanoTime();

            if (current - last >= 50000000) {
                last = current;
                if (tickCounter < 200) tickCounter++;
                else tickCounter = 0;
                handleTick();
            }

            try {
                Thread.sleep(40);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleTick() {

        if (tickCounter == 0) new Thread(new SaveThread(theInterface, slHandler));
        if (tickCounter % 20 == 0) {
            new Thread(checkButtonsThread).start();
            new Thread(new CheckAchievementsThread()).start();
        }
        if (tickCounter % 40 == 0) {
            thisHandmadeCookies = cookieManager.getHandmadeCookies();
            handmadePerSec = (thisHandmadeCookies - lastHandmadeCookies) / 2;
            lastHandmadeCookies = cookieManager.getHandmadeCookies();

            theInterface.updateLabel(HANDMADE_RATE, handmadePerSec);
        }

        theInterface.updateLabel(CURRENT_COOKIES, cookieManager.getCurrentCookies());
        theInterface.updateLabel(HANDMADE_COOKIES, cookieManager.getHandmadeCookies());
        theInterface.updateLabel(TOTAL_COOKIES, cookieManager.getTotalCookies());

        cookieManager.decimalValue += Helper.getCookieRate() / 20;
        if (cookieManager.decimalValue >= 1.0F) {
            float rem = cookieManager.decimalValue - (cookieManager.decimalValue % 1);

            cookieManager.addCookies((int) rem);
            cookieManager.decimalValue -= rem;
        }
    }

    private static void getResources() {
        File images = new File("./images");


        if (!images.exists()) {
            FileDownloaderThread fdt = NetworkUtils.saveURLto("http://www.jmnetwork.ch/public/jm-software/ccassets/images.xml", "./images");
            while (!fdt.isComplete()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
