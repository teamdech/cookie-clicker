/**************************************************************************************************
 * cookieclicker Copyright (C) 2014 Team DE-CH: domi1819 / Joel Messerli                          *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package teamdech.cookieclicker.achievement;

import teamdech.cookieclicker.ui.CCAchievementWindow;

import java.io.Serializable;
import java.util.HashMap;

/**
 * User: joel / Date: 12.01.14 / Time: 20:16
 */
public class AchievementManager implements Serializable {

    private HashMap<EnumAchievement, Boolean> achievedMap = new HashMap<>();

    public AchievementManager() {
        init();
    }

    private void init() {
        for (EnumAchievement achievement : EnumAchievement.values())
            if (achievedMap.get(achievement) == null) achievedMap.put(achievement, false);
    }

    public boolean isAchieved(EnumAchievement achievement) {
        Boolean x = achievedMap.get(achievement);
        return x != null && x;
    }

    protected void achieve(EnumAchievement achievement) {
        if (!isAchieved(achievement)) {
            new CCAchievementWindow(achievement.name, "Bake " + achievement.needed + " cookies.");
            achievedMap.put(achievement, true);
        }
    }
}
