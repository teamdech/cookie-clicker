/**************************************************************************************************
 * cookieclicker Copyright (C) 2014 Team DE-CH: domi1819 / Joel Messerli                          *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package teamdech.cookieclicker.achievement;

/**
 * User: joel / Date: 12.01.14 / Time: 20:16
 */
public enum EnumAchievement {

    WAKE_AND_BAKE(1, "Wake and bake"),
    MAKING_SOME_DOUGH(100, "Making some dough"),
    SO_BAKED(1000, "So baked right now"),
    FLEDGLING_BAKERY(10000, "Fledgling bakery"),
    FFFRZGTAUSEND(40000, "FFFRZIGTAUSEND"),
    AFFLUENT_BAKERY(100000, "Affluent bakery"),
    WORLD_FAMOUS(1000000, "World famous bakery"),
    COSMIC_BAKERY(10000000, "Cosmic bakery"),
    GALACTIC_BAKERY(100000000, "Galactic bakery"),
    UNIVERSAL_BAKERY(1000000000, "Universal bakery"),
    TIMELESS_BAKERY(5000000000L, "Timeless bakery"),
    INFINITE_BAKERY(10000000000L, "Infinite bakery"),
    IMMORTAL_BAKERY(50000000000L, "Immortal bakery"),
    STOP_NOW(100000000000L, "You can stop now"),
    ALL_WAY_DOWN(500000000000L, "Cookies all the way down"),
    OVERDOSE(1000000000000L, "Overdose"),
    HOW(10000000000000L, "How?");

    public long needed;
    public String name;

    EnumAchievement(long needed, String name) {
        this.needed = needed;
        this.name = name;
    }
}