/**************************************************************************************************
 * cookieclicker Copyright (C) 2014 Team DE-CH: domi1819 / Joel Messerli                          *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package teamdech.cookieclicker.threading;

import ch.jmnetwork.core.logging.Logger;
import ch.jmnetwork.core.logging.LogLevel;
import teamdech.cookieclicker.CookieClickerMain;
import teamdech.cookieclicker.helper.EnumHelper;
import teamdech.cookieclicker.helper.Helper;

import java.util.HashMap;

/**
 * User: joel / Date: 10.01.14 / Time: 11:47
 */
public class CheckButtonsThread implements Runnable {

    private HashMap<EnumHelper, BtnState> buttonState = new HashMap<>();
    private Logger logger = CookieClickerMain.instance.logger;

    @Override
    public void run() {
        for (EnumHelper eh : EnumHelper.values()) {
            BtnState old_state = buttonState.get(eh);
            BtnState new_state = new BtnState(Helper.getPriceForHelper(eh) <= CookieClickerMain.instance.cookieManager.getCurrentCookies(), Helper.owned[eh.id], Helper.getPriceForHelper(eh));
            if (new_state.state) {
                if (old_state != null && old_state.equals(new_state)) continue;
                buttonState.put(eh, new BtnState(true, Helper.owned[eh.id], Helper.getPriceForHelper(eh)));
                CookieClickerMain.instance.theInterface.updateButton(eh, new_state.owned, new_state.price, true);
                logger.log(LogLevel.DEBUG, String.format("Updating %s: Owned / Price / Enabled | %d / %d / %s", eh.name, new_state.owned, new_state.price, new_state.state));
            } else {
                if (old_state != null && old_state.equals(new_state)) continue;
                buttonState.put(eh, new BtnState(false, Helper.owned[eh.id], Helper.getPriceForHelper(eh)));
                CookieClickerMain.instance.theInterface.updateButton(eh, new_state.owned, new_state.price, false);
                logger.log(LogLevel.DEBUG, String.format("Updating %s: Owned / Price / Enabled | %d / %d / %s", eh.name, new_state.owned, new_state.price, new_state.state));
            }
        }
    }

    private class BtnState {
        public boolean state;
        public int owned;
        public long price;

        private BtnState(boolean state, int owned, long price) {
            this.state = state;
            this.owned = owned;
            this.price = price;
        }

        @Override
        public boolean equals(Object obj) {
            BtnState state1 = (BtnState) obj;

            return state1.state == state && state1.price == price && state1.owned == owned;
        }
    }
}
