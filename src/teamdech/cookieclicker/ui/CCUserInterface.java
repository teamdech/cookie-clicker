/**************************************************************************************************
 * cookieclicker Copyright (C) 2014 Team DE-CH: domi1819 / Joel Messerli                          *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package teamdech.cookieclicker.ui;

import ch.jmnetwork.core.logging.LogLevel;
import teamdech.cookieclicker.CookieManager;
import teamdech.cookieclicker.helper.EnumHelper;
import teamdech.cookieclicker.helper.Helper;
import teamdech.cookieclicker.lib.Reference;
import teamdech.cookieclicker.util.NumberHelper;
import teamdech.cookieclicker.util.SaveLoadHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;

import static teamdech.cookieclicker.CookieClickerMain.instance;

public class CCUserInterface {
    // ======================================//
    // VARIABLES
    // ======================================//

    private static CookieManager cookiemanager;

    private final static String FONT = "Arial";
    private static String currentInfoMessage = "";
    private static SaveLoadHandler slhandler;
    public final static ImageIcon cookie = instance.slHandler.images[0], cookie_small = instance.slHandler.images[1];

    public static JFrame jframe;
    private static JLabel infoLabel, currentCookiesLabel, cookieRateLabel, cookiesHandmade, cookiesHandmadePerSec, totalCookies;
    private static JButton cookie_button;
    private static HashMap<EnumHelper, JButton> buttonMap = new HashMap<>();

    public CCUserInterface(CookieManager cookieManager, SaveLoadHandler slHandler) {
        cookiemanager = cookieManager;
        slhandler = slHandler;

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    init();
                    jframe.setVisible(true);
                } catch (Exception e) {
                    instance.logger.logStackTrace(LogLevel.ERROR, e);
                }
            }
        });
    }

    private void init() {
        // ======================================//
        // TRY TO SET SYSTEM LOOK AND FEEL
        // ======================================//

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            instance.logger.log(LogLevel.WARNING, "FAIL: Unable to set system look'n'feel");
        }

        // ======================================//
        // DEFINE ALL VARIABLES
        // ======================================//

        jframe = new JFrame();
        ImagePanel contentPane = new ImagePanel(instance.slHandler.images[2].getImage());
        infoLabel = new JLabel();
        cookie_button = new JButton();
        currentCookiesLabel = new JLabel();
        cookieRateLabel = new JLabel();
        cookiesHandmade = new JLabel();
        cookiesHandmadePerSec = new JLabel();
        JButton pointerBuyButton = new JButton();
        JButton grandmaBuyButton = new JButton();
        totalCookies = new JLabel();

        // ======================================//
        // COMPONENT SETTINGS
        // ======================================//

        jframe.setTitle("Java Cookie Clicker by TH3ON1YN00B and domi1819 - Version " + Reference.VERSION);
        jframe.setSize(1000, (265 + 80));
        jframe.getContentPane().setLayout(null);
        jframe.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        jframe.setLocationRelativeTo(null);
        jframe.setResizable(false);
        jframe.setIconImage(cookie.getImage());
        jframe.setContentPane(contentPane);

        jframe.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                slhandler.saveToDisk();
                System.exit(0);
            }
        });

        contentPane.getInputMap().put(KeyStroke.getKeyStroke("typed x"), "cheatUI");
        contentPane.getActionMap().put("cheatUI", new CheatAction());

        infoLabel.setText("");
        infoLabel.setFont(Font.getFont(FONT));
        infoLabel.setBounds(40 + 265, 270, 300, 50);
        infoLabel.setForeground(Color.RED);

        cookie_button.setBounds(20, 20, 265, 265);
        cookie_button.setBorder(BorderFactory.createEmptyBorder());
        cookie_button.setContentAreaFilled(false);
        cookie_button.setFocusPainted(false);
        cookie_button.setIcon(cookie);
        cookie_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cookiemanager.addHandmadeCookies(1);
            }
        });
        cookie_button.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent arg0) {
                if (arg0.getButton() == MouseEvent.BUTTON1) cookie_button.setIcon(cookie);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent arg0) {
                if (arg0.getButton() == MouseEvent.BUTTON1) cookie_button.setIcon(cookie_small);
            }
        });

        currentCookiesLabel.setText("Current Cookies");
        currentCookiesLabel.setFont(Font.getFont(FONT));
        currentCookiesLabel.setBounds(300, 20, 300, 50);

        cookieRateLabel.setText("Cookie Rate");
        cookieRateLabel.setFont(Font.getFont(FONT));
        cookieRateLabel.setBounds(300, 30, 300, 50);

        cookiesHandmade.setText("Handmade Cookies");
        cookiesHandmade.setFont(Font.getFont(FONT));
        cookiesHandmade.setBounds(300, 40, 300, 50);

        cookiesHandmadePerSec.setText("Handmade Per Second");
        cookiesHandmadePerSec.setFont(Font.getFont(FONT));
        cookiesHandmadePerSec.setBounds(300, 50, 300, 50);

        // ======================================//
        // ALL HELPERS
        // ======================================//

        int _cnt = 0;
        for (EnumHelper helper : EnumHelper.values()) {

            JButton btn = new JButton();
            btn.setText(helper.name);
            btn.setBounds(600, _cnt * 22 + 20, 350, 20);
            btn.setEnabled(false);
            btn.setToolTipText("Produces " + helper.productivity + " Cookies / s");

            final EnumHelper tempHelper = helper;
            btn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    EnumHelper eh = EnumHelper.values()[tempHelper.id];

                    if (cookiemanager.buyPrice(Helper.getPriceForHelper(tempHelper))) {
                        Helper.onBought(eh);
                    }
                }
            });

            buttonMap.put(helper, btn);
            jframe.getContentPane().add(btn);
            _cnt++;
        }

        totalCookies.setText("TOTAL_HERE");
        totalCookies.setFont(Font.getFont(FONT));
        totalCookies.setBounds(300, 70, 300, 40);

        // ======================================//
        // ADD COMPONENTS TO THE PANE
        // ======================================//

        jframe.getContentPane().add(cookie_button);
        jframe.getContentPane().add(currentCookiesLabel);
        jframe.getContentPane().add(cookieRateLabel);
        jframe.getContentPane().add(cookiesHandmade);
        jframe.getContentPane().add(cookiesHandmadePerSec);
        jframe.getContentPane().add(pointerBuyButton);
        jframe.getContentPane().add(grandmaBuyButton);
        jframe.getContentPane().add(infoLabel);
        jframe.getContentPane().add(totalCookies);
    }

    public void updateButton(EnumHelper helper, int ammount, long price, boolean active) {
        JButton btn = buttonMap.get(helper);

        btn.setText(String.format("%d %ss, Price: %d C", ammount, helper.name, price));
        btn.setEnabled(active);
    }

    public void updateLabel(LabelType labelType, long newValue) {
        switch (labelType) {
            case CURRENT_COOKIES:
                if (currentCookiesLabel != null)
                    currentCookiesLabel.setText("Current Cookies: " + NumberHelper.addApostropheToNumber(newValue));
                break;
            case HANDMADE_COOKIES:
                if (cookiesHandmade != null)
                    cookiesHandmade.setText("Handmade Cookies: " + NumberHelper.addApostropheToNumber(newValue));
                break;
            case TOTAL_COOKIES:
                if (totalCookies != null)
                    totalCookies.setText("All-Time Cookies: " + NumberHelper.addApostropheToNumber(newValue));
                break;
            case COOKIE_RATE:
                if (cookieRateLabel != null)
                    cookieRateLabel.setText("Current Cookie Rate: " + onlyOneAfterComma(newValue));
                break;
            case HANDMADE_RATE:
                if (cookiesHandmadePerSec != null) cookiesHandmadePerSec.setText("Handmade Cookies / s: " + newValue);
                break;
        }
    }

    public void updateLabel(LabelType labelType, float newValue) {
        switch (labelType) {
            case COOKIE_RATE:
                if (cookieRateLabel != null)
                    cookieRateLabel.setText("Current Cookie Rate: " + onlyOneAfterComma(newValue));
                break;
        }
    }

    public void setInfoMessage(String message) {
        infoLabel.setText("<html><strong>" + message + "</strong></html>");
        currentInfoMessage = message;
    }

    public String getInfoMessage() {
        return currentInfoMessage;
    }

    private float onlyOneAfterComma(float input) {
        return (float) (Math.floor(input * 10F) / 10F);
    }

    public enum LabelType {
        CURRENT_COOKIES, HANDMADE_COOKIES, TOTAL_COOKIES, COOKIE_RATE, HANDMADE_RATE
    }

    private class CheatAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {

            CheatAddCookiesDialog dialog = new CheatAddCookiesDialog(cookiemanager);
            dialog.pack();
            dialog.setVisible(true);
        }
    }
}
