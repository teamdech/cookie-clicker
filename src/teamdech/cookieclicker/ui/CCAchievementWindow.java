/**************************************************************************************************
 * cookieclicker Copyright (C) 2014 Team DE-CH: domi1819 / Joel Messerli                          *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package teamdech.cookieclicker.ui;

import teamdech.cookieclicker.CookieClickerMain;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CCAchievementWindow {
    // ======================================//
    // VARIABLES
    // ======================================//

    JFrame ccui;
    Point mouseDownPoint;
    JFrame jframe = new JFrame();
    JLabel achievementName = new JLabel();
    JLabel achievementDescription = new JLabel();
    JButton exitButton = new JButton();

    public CCAchievementWindow(String achievementname, String achievementdescription) {
        achievementName.setHorizontalAlignment(SwingConstants.CENTER);
        achievementName.setText(achievementname);
        achievementDescription.setText(achievementdescription);
        this.ccui = CCUserInterface.jframe;
        init();
        jframe.setOpacity(0F);
        jframe.setVisible(true);

        new Thread(new VisibilityThread(jframe)).start();
    }

    private void init() {
        // ======================================//
        // SETTNGS
        // ======================================//

        jframe.setSize(500, 200);
        jframe.setTitle("Achievement aquired!");
        try {
            jframe.setLocation((int) ccui.getLocationOnScreen().getX() + 250, (int) ccui.getLocationOnScreen().getY() - 210);
        } catch (IllegalComponentStateException e) {
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            jframe.setLocation(d.width - 550, d.height - 250);
        }

        jframe.setUndecorated(true);
        jframe.getContentPane().setLayout(null);
        jframe.setOpacity(1.0F);
        jframe.setIconImage(CCUserInterface.cookie.getImage());
        jframe.setContentPane(new ImagePanel(CookieClickerMain.instance.slHandler.images[3].getImage()));
        addJFrameMouseListener();

        achievementName.setBounds(20, 5, 480, 50);
        achievementName.setForeground(Color.red);
        achievementName.setFont(new Font("Arial", Font.BOLD, 24));
        achievementName.setText("<html><strong>" + achievementName.getText() + "</strong></html>");
        achievementDescription.setBounds(20, 60, 480, 20);

        exitButton.setText("");
        exitButton.setBounds(420, 160, 70, 30);
        exitButton.setContentAreaFilled(false);
        exitButton.setBorder(BorderFactory.createEmptyBorder());
        exitButton.setFocusPainted(false);
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                jframe.setVisible(false);
            }
        });

        // ======================================//
        // ADD TO THE PANE
        // ======================================//

        jframe.getContentPane().add(exitButton);
        jframe.getContentPane().add(achievementName);
        jframe.getContentPane().add(achievementDescription);
    }

    private void addJFrameMouseListener() {
        jframe.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent arg0) {
                mouseDownPoint = null;
            }

            @Override
            public void mousePressed(MouseEvent arg0) {
                mouseDownPoint = arg0.getPoint();
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
            }

            @Override
            public void mouseClicked(MouseEvent arg0) {
            }
        });

        jframe.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseMoved(MouseEvent e) {
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                Point currentCoords = e.getLocationOnScreen();
                jframe.setLocation(currentCoords.x - mouseDownPoint.x, currentCoords.y - mouseDownPoint.y);
            }
        });
    }

    class VisibilityThread implements Runnable {
        JFrame jf;

        public VisibilityThread(JFrame JFrame) {
            jf = JFrame;
        }

        @Override
        public void run() {
            for (float i = 0F; i <= 1.0F; i += 0.05F) {
                jf.setOpacity(i);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
