/**************************************************************************************************
 * cookieclicker Copyright (C) 2014 Team DE-CH: domi1819 / Joel Messerli                          *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package teamdech.cookieclicker.util;

import ch.jmnetwork.core.logging.LogLevel;
import ch.jmnetwork.core.storage.StorageHandler;
import ch.jmnetwork.core.storage.StorageObject;
import teamdech.cookieclicker.CookieClickerMain;
import teamdech.cookieclicker.CookieManager;
import teamdech.cookieclicker.achievement.AchievementManager;
import teamdech.cookieclicker.helper.Helper;

import javax.swing.*;
import java.io.File;

public class SaveLoadHandler {
    private static CookieManager cookiemanager;
    public static StorageHandler storageHandler = new StorageHandler(new File("./cookieclicker.xml"));
    private static StorageHandler imagesHandler = new StorageHandler(new File("./images"));

    /**
     * All the images used in cookieclicker
     * <ul>
     * <li>0 - cookie, big</li>
     * <li>1 - cookie, small</li>
     * <li>2 - UI Bcakground</li>
     * <li>3 - Achievement Background</li>
     * </ul>
     */
    public ImageIcon[] images = imagesHandler.getStorageObjectByName("images", ImageIcon[].class).getObject();

    public SaveLoadHandler(CookieManager cm) {
        cookiemanager = cm;
    }

    public void saveToDisk() {
        storageHandler.setProperty("current_cookies", cookiemanager.getCurrentCookies(), Long.class);
        storageHandler.setProperty("handmade_cookies", cookiemanager.getHandmadeCookies(), Long.class);
        storageHandler.setProperty("total_cookies", cookiemanager.getTotalCookies(), Long.class);

        storageHandler.setProperty("achievement_manager", CookieClickerMain.instance.achievementManager, AchievementManager.class);

        storageHandler.setProperty("objects_owned", Helper.owned, int[].class);

        //storageHandler.setProperty("images", new ImageIcon[]{new ImageIcon("cookie.png"), new ImageIcon("cookie_small.png"), new ImageIcon("BackgroundNew.png"), new ImageIcon("AchievementBackground.png")}, ImageIcon[].class);

        storageHandler.saveStorage();
    }

    public void loadFromDisk() {

        StorageObject<Long> current_cookies = storageHandler.getStorageObjectByName("current_cookies", Long.class);
        StorageObject<Long> handmade_cookies = storageHandler.getStorageObjectByName("handmade_cookies", Long.class);
        StorageObject<Long> total_cookies = storageHandler.getStorageObjectByName("total_cookies", Long.class);
        StorageObject<int[]> owned = storageHandler.getStorageObjectByName("objects_owned", int[].class);
        StorageObject<AchievementManager> achievementManager = storageHandler.getStorageObjectByName("achievement_manager", AchievementManager.class);

        // ======================================//
        // FIRST TIME CHECK
        // ======================================//

        if (current_cookies == null || handmade_cookies == null || total_cookies == null || owned == null || achievementManager == null)
            return;

        // ======================================//
        // GET DATA
        // ======================================//

        Long c_cookies = current_cookies.getObject();
        Long h_cookies = handmade_cookies.getObject();
        Long t_cookies = total_cookies.getObject();

        int[] helpers_owned = owned.getObject();

        AchievementManager manager = achievementManager.getObject();

        CookieClickerMain.instance.logger.log(LogLevel.DEBUG, String.format("c_c: %d | h_c: %d | t_c: %d", c_cookies, h_cookies, t_cookies));

        // ======================================//
        // UPDATE GAME DATA
        // ======================================//

        cookiemanager.setCurrentCookies(c_cookies);
        cookiemanager.setHandmadeCookies(h_cookies);
        cookiemanager.setTotalCookies(t_cookies);

        Helper.owned = helpers_owned;

        CookieClickerMain.instance.achievementManager = manager;
    }
}
