/**************************************************************************************************
 * cookieclicker Copyright (C) 2014 Team DE-CH: domi1819 / Joel Messerli                          *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package teamdech.cookieclicker.util;

public class NumberHelper {

    public static String addApostropheToNumber(Long number) {
        String returnValue = "";
        String number_string = number + "";
        int times = 0;

        for (int i = 0; i < number_string.length(); i++) {
            if (i % 3 == 0 && i != 0) {
                int from = number_string.length() - i;
                String split = number_string.substring(from, from + 3);
                returnValue = "'" + split + returnValue;
                times++;
            }
        }
        if (returnValue.length() != number_string.length() + times) {
            returnValue = number_string.substring(0, number_string.length() + times - returnValue.length()) + returnValue;
        }

        return returnValue;
    }
}
