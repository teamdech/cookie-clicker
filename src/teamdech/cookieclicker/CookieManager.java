/**************************************************************************************************
 * cookieclicker Copyright (C) 2014 Team DE-CH: domi1819 / Joel Messerli                          *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package teamdech.cookieclicker;

public class CookieManager {
    private long cookiesTotal = 0, cookiesCurrent = 0, cookiesHandmade = 0;
    public float decimalValue;

    public void addHandmadeCookies(int cookiesAmount) {
        cookiesHandmade += cookiesAmount;
        addCookies(cookiesAmount);
    }

    public long getHandmadeCookies() {
        return cookiesHandmade;
    }

    public void addCookies(int cookiesAmount) {
        addCookies((long) cookiesAmount);
    }

    public void addCookies(long cookiesAmount) {
        if (cookiesCurrent + cookiesAmount < Long.MAX_VALUE) {
            cookiesTotal += cookiesAmount;
            cookiesCurrent += cookiesAmount;
        }
    }

    public long getCurrentCookies() {
        return cookiesCurrent;
    }

    public boolean buyPrice(long cookiesPrice) {
        if (cookiesCurrent >= cookiesPrice) {
            cookiesCurrent -= cookiesPrice;
            return true;
        } else {
            return false;
        }
    }

    public long getTotalCookies() {
        return cookiesTotal;
    }

    /**
     * PLEASE ONLY USE FOR SAVE / LOAD PROCESS
     *
     * @param cookiestotal
     */
    public void setTotalCookies(long cookiestotal) {
        this.cookiesTotal = cookiestotal;
    }

    /**
     * PLEASE ONLY USE FOR SAVE / LOAD PROCESS
     *
     * @param currentCookies
     */
    public void setCurrentCookies(long currentCookies) {
        this.cookiesCurrent = currentCookies;
    }

    /**
     * PLEASE ONLY USE FOR SAVE / LOAD PROCESS
     *
     * @param cookiesTotal
     */
    public void setHandmadeCookies(long cookiesTotal) {
        cookiesHandmade = cookiesTotal;
    }
}
