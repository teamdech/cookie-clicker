/**************************************************************************************************
 * cookieclicker Copyright (C) 2014 Team DE-CH: domi1819 / Joel Messerli                          *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package teamdech.cookieclicker.helper;

import java.util.ArrayList;

import static teamdech.cookieclicker.util.NumberHelper.addApostropheToNumber;

public enum EnumHelper {
    CLICKER(0.1F, 15, 0, "Pointer"),
    GRANDMA(0.5F, 100, 1, "Grandma"),
    FARM(2F, 500, 2, "Farm"),
    FACTORY(10F, 3000, 3, "Factory"),
    MINE(40F, 10000, 4, "Mine"),
    SHIPMENT(100F, 40000, 5, "Shipment"),
    ALCHEMYLAB(400F, 200000, 6, "Alchemy Lab"),
    PORTAL(6666F, 1666666, 7, "Portal"),
    TIMEMACHINE(98765F, 123456789L, 8, "Time Machine"),
    CONDENSER(999999F, 3999999999L, 9, "Condenser");

    public final long price;
    public final float productivity;
    public final int id;
    public final String name;

    private static ArrayList<String[]> helpersStringArrayThing = null;

    private EnumHelper(float prodictivity, long price, int ID, String name) {
        this.productivity = prodictivity;
        this.price = price;
        this.id = ID;
        this.name = name;
    }

    public static ArrayList<String[]> getAllHelpersAsStringArray() {
        if (helpersStringArrayThing != null) return helpersStringArrayThing;
        ArrayList<String[]> r_list = new ArrayList<>();

        r_list.add(new String[]{"ID", "Name", "Productivity", "Price"});
        for (EnumHelper e : values()) {
            r_list.add(new String[]{e.id + "", e.name, (e.productivity + "").substring((e.productivity + "").indexOf(".")).equals(".0") ? addApostropheToNumber(Long.parseLong((e.productivity + "").substring(0, (e.productivity + "").indexOf(".")))) : (e.productivity + ""), addApostropheToNumber(e.price) + ""});
        }

        helpersStringArrayThing = r_list;
        return r_list;
    }
}
