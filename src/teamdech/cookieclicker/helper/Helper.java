/**************************************************************************************************
 * cookieclicker Copyright (C) 2014 Team DE-CH: domi1819 / Joel Messerli                          *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package teamdech.cookieclicker.helper;

import static teamdech.cookieclicker.CookieClickerMain.instance;
import static teamdech.cookieclicker.ui.CCUserInterface.LabelType.COOKIE_RATE;

public class Helper {
    private static float cookieRate = 0;
    private static boolean cookierateNeedsRecalc = true;
    public static int[] owned = getOwnedArray();

    public static void onBought(EnumHelper helper) {
        owned[helper.id]++;
        cookierateNeedsRecalc = true;
        instance.theInterface.updateLabel(COOKIE_RATE, getCookieRate());
        new Thread(instance.checkButtonsThread).start();
    }

    public static float getCookieRate() {
        if (cookieRate != 0 && !cookierateNeedsRecalc) return cookieRate;
        float returnValue = 0;

        EnumHelper[] t_helpers = EnumHelper.values();

        for (int i = 0; i < t_helpers.length; i++)
            returnValue += t_helpers[i].productivity * owned[i];

        cookieRate = returnValue;

        cookierateNeedsRecalc = false;
        return returnValue;
    }

    public static long getPriceForHelper(EnumHelper helper) {
        return Helper.getPriceForNextHelper(helper);
    }

    private static long getPriceForNextHelper(EnumHelper helper) {
        return (long) (helper.price * Math.pow(1.15F, owned[helper.id]));
    }

    private static int[] getOwnedArray() {
        int[] arr = new int[EnumHelper.values().length];

        int _cnt = 0;
        for (int i : arr) {
            arr[_cnt] = 0;
            _cnt++;
        }

        return arr;
    }
}
